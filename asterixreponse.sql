use asterixdb

SELECT "Liste des potions : Numéro, libellé, formule et constituant principal." AS QUESTION_1;

SELECT * FROM potion;

SELECT "Liste des noms des trophées rapportant 3 points." AS QUESTION_2;

SELECT * FROM categorie
    WHERE NbPoints = '3';

SELECT "Liste des villages (noms) contenant plus de 35 huttes." AS QUESTION_3;

SELECT * FROM village
    WHERE NbHuttes > 35;

SELECT "Liste des trophées (numéros) pris en mai / juin 52." AS QUESTION_4;

SELECT * FROM trophee
    WHERE YEAR(DatePrise) = 2052 AND (MONTH(DatePrise) = 5 OR MONTH(DatePrise) = 6);

SELECT "Noms des habitants commençant par 'a' et contenant la lettre 'r'." AS QUESTION_5;

SELECT * FROM habitant
    WHERE Nom like 'a%r%';

SELECT "Numéros des habitants ayant bu les potions numéros 1, 3 ou 4." AS QUESTION_6;

SELECT NumHab FROM absorber
    WHERE NumPotion IN (1, 3, 4);

SELECT "Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur." AS QUESTION_7;

SELECT t.NumTrophee, t.DatePrise, c.NomCateg, h.Nom FROM trophee t
INNER JOIN categorie c ON t.CodeCat = c.CodeCat
INNER JOIN habitant h ON t.NumPreneur = h.NumHab;

SELECT "Nom des habitants qui habitent à Aquilona." AS QUESTION_8;

SELECT Nom FROM habitant
INNER JOIN village ON habitant.NumVillage = village.NumVillage
WHERE village.NomVillage = 'Aquilona';

SELECT "Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat." AS QUESTION_9;

SELECT Nom FROM habitant
INNER JOIN trophee on trophee.NumPreneur = habitant.NumHab
INNER JOIN categorie on categorie.CodeCat = trophee.CodeCat
WHERE categorie.NomCateg = 'Bouclier de Légat';

SELECT "Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant
principal." AS QUESTION_10;

SELECT LibPotion, Formule, ConstituantPrincipal FROM potion
INNER JOIN fabriquer on potion.NumPotion = fabriquer.NumPotion
INNER JOIN habitant on fabriquer.NumHab = habitant.NumHab
WHERE habitant.Nom = 'Panoramix';

SELECT "Liste des potions (libellés) absorbées par Homéopatix." AS QUESTION_11;

SELECT potion.LibPotion FROM potion
INNER JOIN absorber ON absorber.NumPotion = potion.NumPotion
INNER JOIN habitant ON absorber.NumHab = habitant.NumHab
WHERE habitant.Nom = 'Homéopatix';

SELECT "Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro
3." AS QUESTION_12;

SELECT DISTINCT habitant.Nom FROM habitant
INNER JOIN absorber ON habitant.NumHab = absorber.NumHab
INNER JOIN fabriquer ON absorber.NumPotion = fabriquer.NumPotion
WHERE fabriquer.NumHab = 3;

SELECT "Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix." AS QUESTION_13;

SELECT DISTINCT habitant.Nom FROM habitant
INNER JOIN absorber ON absorber.NumHab = habitant.NumHab
INNER JOIN potion ON potion.NumPotion = absorber.NumPotion
INNER JOIN fabriquer ON potion.NumPotion = fabriquer.NumPotion
WHERE fabriquer.NumHab = 2;

SELECT "14. Nom des habitants dont la qualité n'est pas renseignée" AS QUESTION_14;

SELECT Nom
FROM habitant
WHERE NumQualite IS NULL;

SELECT "Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la
potion) en février 52." AS QUESTION_15;

SELECT h.Nom FROM habitant h
JOIN absorber a ON h.NumHab = a.NumHab
JOIN potion p ON a.NumPotion = p.NumPotion
WHERE p.LibPotion = 'Potion magique n°1' AND MONTH(a.DateA) = 2 AND YEAR(a.DateA) = 2052;

SELECT "Nom et âge des habitants par ordre alphabétique." AS QUESTION_16;

SELECT Nom, Age
FROM habitant
ORDER BY Nom;

SELECT "Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom
du village." QUESTION_17;

SELECT r.NomResserre, v.NomVillage FROM resserre r
JOIN village v ON r.NumVillage = v.NumVillage
ORDER BY r.Superficie DESC;

SELECT "Nombre d'habitants du village numéro 5." QUESTION_18;

SELECT COUNT(*) AS Habitants_Village_5 FROM habitant
WHERE NumVillage = 5;

SELECT "Nombre de points gagnés par Goudurix." QUESTION_19;

SELECT SUM(c.NbPoints) AS Points_Gagnes_Par_Goudurix FROM habitant h
INNER JOIN qualite q ON h.NumQualite = q.NumQualite
INNER JOIN trophee t ON h.NumHab = t.NumPreneur
INNER JOIN categorie c ON t.CodeCat = c.CodeCat
WHERE h.Nom = 'Goudurix';

SELECT "Date de première prise de trophée." QUESTION_20;

SELECT MIN(DatePrise) AS Date_de_Premiere_Prise_de_Trophee
FROM trophee;

SELECT "Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées." AS QUESTION_21;

SELECT SUM(Quantite) AS Nombre_de_Louches FROM absorber a
INNER JOIN potion p ON a.NumPotion = p.NumPotion
WHERE p.LibPotion = 'Potion magique n°2';

SELECT "Superficie la plus grande." AS QUESTION_22;

SELECT MAX(Superficie) AS Superficie_la_Plus_Grande
FROM resserre;

SELECT "Nombre d'habitants par village (nom du village, nombre)." AS QUESTION_23;

SELECT v.NomVillage AS Nom_Du_Village, COUNT(h.NumHab) AS Nombre FROM village v
LEFT JOIN habitant h ON v.NumVillage = h.NumVillage
GROUP BY v.NomVillage;

SELECT "Nombre de trophées par habitant." AS QUESTION_24;

SELECT h.Nom AS Nom_Habitants,
COUNT(*) AS Nombre_de_trophees FROM habitant h 
RIGHT JOIN trophee t ON h.NumHab = t.NumPreneur
GROUP BY h.Nom;

SELECT "Moyenne d'âge des habitants par province (nom de province, calcul)." AS QUESTION_25;

SELECT p.NomProvince AS Nom_De_Province, AVG(h.Age) AS Moyenne_Age
FROM habitant h
INNER JOIN village v ON h.NumVillage = v.NumVillage
INNER JOIN province p ON v.NumProvince = p.NumProvince
GROUP BY p.NomProvince;

SELECT "Nombre de potions différentes absorbées par chaque habitant (nom et nombre)." AS QUESTION_26;

SELECT h.Nom AS Nom_Habitant, COUNT(DISTINCT a.NumPotion) AS Nombre_De_Potions_Differentes
FROM habitant h
RIGHT JOIN absorber a ON h.NumHab = a.NumHab
GROUP BY h.Nom;

SELECT "Nom des habitants ayant bu plus de 2 louches de potion zen." AS QUESTION_27;

SELECT h.Nom AS Nom_habitant FROM habitant h
INNER JOIN absorber a ON h.NumHab = a.NumHab
INNER JOIN potion p ON a.NumPotion = p.NumPotion
WHERE p.LibPotion = 'Potion Zen' AND a.Quantite > 2;

SELECT "Noms des villages dans lesquels on trouve une resserre." AS QUESTION_28;

SELECT DISTINCT v.NomVillage AS Nom_Du_Village FROM village v
INNER JOIN resserre r ON v.NumVillage = r.NumVillage;

SELECT "Nom du village contenant le plus grand nombre de huttes." AS QUESTION_29;

SELECT v.NomVillage AS Nom_Du_Village FROM village v
WHERE v.NbHuttes = (SELECT MAX(NbHuttes) FROM village);

SELECT "Noms des habitants ayant pris plus de trophées qu'Obélix'." AS QUESTION_30;

SELECT Nom, Count(NumTrophee) AS Nombre_de_trophees FROM habitant
JOIN trophee ON trophee.NumPreneur = habitant.NumHab
GROUP BY Nom
HAVING  Count(NumTrophee) > (SELECT Count(NumTrophee) FROM trophee 
JOIN habitant ON trophee.NumPreneur = habitant.NumHab
WHERE habitant.Nom = "Obélix");

